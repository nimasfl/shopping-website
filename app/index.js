const express = require('express');
const app = express();
const http = require('http');
const bodyParser = require('body-parser');
const path = require('path');
const cookieParser = require('cookie-parser');
const validator = require('express-validator');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const mongoose = require('mongoose');
const flash = require('connect-flash');
const passport = require('passport');
const webRoutes = require('./routes/web');
const apiRoutes = require('./routes/api');

module.exports = class Application {
    constructor(){
        this.setupExpress();
        this.setMongoConnection();
        this.seConfig();
        this.setRouters();
    }

    setupExpress(){
        const server = http.createServer(app);
        server.listen(3333, ()=> console.info('Listening on port 3333 ...'));
    }
    // Express configurations
    seConfig(){
        app.use(express.static('public'));
        app.set('view engine', 'ejs');
        app.set('views', path.resolve('./resource/views'));
        app.use(session({
            secret: "secretkey",
            resave: true,
            saveUninitialized : true,
            store: new MongoStore({
                mongooseConnection: mongoose.connection
            })
        }));
        app.use(cookieParser('secretkey'));
        app.use(flash());
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({extended : true}));
        app.use(validator());
    }
    setMongoConnection(){
        mongoose.Promise = global.Promise;
        mongoose.connect('mongodb://localhost/nodejscms',{ useNewUrlParser: true });
    }
    setRouters(){
        app.use(webRoutes);
        app.use(apiRoutes);
    }
};
