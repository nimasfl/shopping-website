const express = require('express');
const router = express.Router();

// Admin Router
const adminRouter = require('./admin');
router.use('/admin', adminRouter);

// Home router
const homeRouter = require('./home');
router.use('/', homeRouter);

module.exports = router;
