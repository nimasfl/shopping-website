const express = require('express');
const router = express.Router();
//Controllers
const adminController = require('app/http/controllers/admin/adminController');

// Admin Routes
router.get('/', adminController.index.bind(adminController));
router.get('/courses', adminController.courses.bind(adminController));

module.exports = router;
