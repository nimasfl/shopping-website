const autoBind = require('auto-bind');
const Recaptcha = require('express-recaptcha').Recaptcha;

module.exports = class controller {
    constructor(){
        autoBind(this);
        this.recaptchaConfig()
    }
    recaptchaConfig() {
        this.recaptcha = new Recaptcha('6LcdHIsUAAAAAN1zkOiqSNznM1InkOyTMXOn-5E0',
            '6LcdHIsUAAAAANboT_XjsZB5Vw4K8EpAr6312snI',
            {hl: 'fa'}
        );
    }
    recaptchaValidation(req, res) {
        return new Promise((resolve, reject) => {
            this.recaptcha.verify(req, (err, data)=>{
                if (err){
                    req.flash('errors', 'گزینه ی امنیتی مربوط به شناسایی ربات خاموش است. لطفا از فعال بودن آ اطمینان حاصل فرمائید سپس مجددا تلاش کنید.')
                    res.redirect(req.url);
                } else {
                    resolve(true);
                }
            })
        })
    }
};

