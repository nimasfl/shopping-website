const controller = require('app/http/controllers/controller');

class registerController extends controller {
    showRegisterForm(req, res){
        res.render('auth/register', { messages: req.flash('errors'), recaptcha: this.recaptcha.render() });
    }
    registerProcess(req, res, next){
        this.recaptchaValidation(req, res)
            .then(result => this.validationData(req, res))
            .then(result => {
                if (result) res.json('register process');
                else res.redirect('/register');
            })
            .catch(err=> console.error(err));

    }
    validationData(req){
        req.checkBody('name', 'فیلد نام نمی تواند خالی بماند.').notEmpty();
        req.checkBody('name', 'فیلد نام نمی تواند کمتر از 4 کارکتر باشد.').isLength({min: 4});
        req.checkBody('email', 'فیلد ایمیل نمی تواند خالی بماند.').notEmpty();
        req.checkBody('email', 'فیلد ایمیل نامعتبر است.').isEmail();
        req.checkBody('password', 'فیلد پسورد نمی تواند خالی بماند.').notEmpty();
        req.checkBody('password', 'فیلد پسورد نمی تواند کم تر از 4 کارکتر باشد.').isLength({min: 4});

        return req.getValidationResult()
            .then(result=>{
                const errors = result.array();
                const messages =[];
                errors.forEach(err=> messages.push(err.msg));
                if (errors.length === 0){
                    return true;
                }
                req.flash('errors', messages);
                return false;

            })
            .catch(err => {
                return err
            })
    }
}

module.exports = new registerController();
