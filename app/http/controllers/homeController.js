const controller = require('app/http/controllers/controller');

class homeController extends controller {
    index(req, res){
        res.render('home');
    }
    aboutUs(req, res){
        res.render('aboutUs');
    }
    contactUs(req, res){
        res.render('contactUs');
    }
}

module.exports = new homeController();
